# Application Requirements

Aplikacia si nacita namockovane data, napriklad z JSONu. Bude to pole objektov, ktore reprezentuju podujatia. Podujatia su vizualne rozdelene medzi nastavajuce, prave beziace a minule. V podujatiach je mozne vyhladavat podla nazvu, popisu podujatia, alebo lokacie. Podujatia je mozne filtrovat podla ich vlastnika, tj. mozem byt vlastnikom podujatia ja, alebo nejaky iny pouzivatel. Pod zoznamom podujati existuje moznost vytvorenia noveho podujatia (Staci nazov, zbytok sa vygeneruje automaticky). Podujatia mozno mazat, duplikovat, pripadne vojst do ich detailu (netreba samostatny screen). Aplikacia pouziva FLUX architekturu.

Odporucane citanie:

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [TypeScript](https://www.typescriptlang.org/)
- [TypeScriptGuide](https://basarat.gitbooks.io/typescript/content/docs/getting-started.html)
- [Angular](https://angular.io/)
- [Webpack](https://webpack.js.org/concepts/)
- [NgRx](https://github.com/ngrx/platform)

a ine.

Kreativite sa medze nekladu, no je dolezite, aby kod bol prehladny a citatelny a aby bolo jasne o co v nom ide.
Angular, aj NgRx pouzivaju nejake zakladne standardy, ktorych je dobre sa drzat (aspon z casti).