import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth-guard.service';
import { CoreModule } from './modules/core/core.module';
import { EventsModule } from './modules/events/events.module';
import { UtilsModule } from './modules/utils/utils.module';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent
  ],
  imports: [
    // Angular modules
    BrowserModule,
    BrowserAnimationsModule,
    // Internal modules
    AppRoutingModule,
    CoreModule,
    EventsModule,
    UtilsModule
  ],
  providers: [AuthGuard]
})
export class AppModule { }
