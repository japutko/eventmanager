import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth-guard.service';
import { ErrorPageComponent } from './modules/core/error-page/error-page.component';
import { LoginComponent } from './modules/core/login/login.component';
import { EventDetailComponent } from './modules/events/event-detail/event-detail.component';
import { EventEditComponent } from './modules/events/event-edit/event-edit.component';
import { EventsComponent } from './modules/events/events.component';

// tslint:disable
const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'events', component: EventsComponent, canActivate: [AuthGuard] },  
    { path: 'events/:id', component: EventDetailComponent, canActivate: [AuthGuard] },
    { path: 'events/:id/edit', component: EventEditComponent, canActivate: [AuthGuard] },
    { path: '404', component: ErrorPageComponent },
    { path: '**', redirectTo: '/404' }
];
// tslint:enable

@NgModule({
    exports: [
        RouterModule
    ],
    imports: [
        RouterModule.forRoot(routes)
    ]
})
export class AppRoutingModule {}
