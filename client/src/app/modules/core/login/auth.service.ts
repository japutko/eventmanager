import { Injectable } from '@angular/core';
import { User } from './user.model';
import { UserService } from './user.service';

@Injectable()
export class AuthService {

    constructor(private userService: UserService) {}

    private loggedIn: boolean = false;

    isAuthenticated(): boolean {
        return this.loggedIn;
    }

    login(user: User): boolean {
        if (this.userService.userExists(user) &&
            this.userService.getUserByName(user.name).password === user.password) {
                this.loggedIn = true;
                return true;
        }
        return false;
    }

    logout(): void {
        this.loggedIn = false;
        this.userService.setActiveUser(null);
    }
}
