import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from './user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  user: User;
  wrongCredentials: boolean = false;
  missingField: boolean = false;
  private loginForm: FormGroup;

  constructor(private router: Router,
              private userService: UserService,
              private authService: AuthService) { }

  ngOnInit() {
    this.user = {name: '', password: ''};
    this.loginForm = this.buildForm();
  }

  login(): void {
    const userName = this.loginForm.get('name').value;
    const pass = this.loginForm.get('password').value;
    if (this.loginForm.valid) {
      if (this.authService.login({name: userName, password: pass})) {
        this.user.name = userName;
        this.user.password = pass;
        this.userService.setActiveUser(this.user);
        this.router.navigate(['/events']);
      } else {
        this.missingField = false;
        this.wrongCredentials = true;
      }
    } else {
      this.wrongCredentials = false;
      this.missingField = true;
    }
    this.loginForm.get('name').markAsTouched();
    this.loginForm.get('password').markAsTouched();
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      name: new FormControl(this.user.name, [Validators.required]),
      password: new FormControl(this.user.password, [Validators.required])
    });
  }
}
