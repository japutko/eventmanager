import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import mocks from '../../../../mockdata/users.json';
import { User } from './user.model';

@Injectable()
export class UserService {

  // Local storage for users
  private users: Array<User>;

  private activeUserSubject = new BehaviorSubject<User>({name: ''});

  constructor() {
    this.users = new Array();
    this.users.push(...mocks);
  }

  getActiveUser(): Observable<User> {
    return this.activeUserSubject.asObservable();
  }

  getUserByName(name: string): User {
    return this.users.find((user) => user.name === name);
  }

  setActiveUser(user: User): void {
    this.activeUserSubject.next(user);
  }

  addUser(user: User): void {
    this.users.push(user);
  }

  removeUser(user: User): void {
    const index = this.users.findIndex( (i) => i === user );
    if (index > -1) {
        this.users.splice(index, 1);
    }
  }

  userExists(user: User): boolean {
    return this.users.findIndex((u) => u.name === user.name) > -1;
  }
}
