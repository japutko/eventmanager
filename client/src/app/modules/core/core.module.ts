import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from './login/auth.service';
import { LoginComponent } from './login/login.component';
import { UserService } from './login/user.service';

@NgModule({
  declarations: [
    ErrorPageComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent
  ],
  exports: [
    ErrorPageComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // PrimeNG modules
    ButtonModule
  ],
  providers: [
    AuthService,
    UserService
  ]
})
export class CoreModule { }
