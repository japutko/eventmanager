import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  styleUrls: ['./error-page.component.scss'],
  templateUrl: './error-page.component.html'
})
export class ErrorPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {}

  goHome() {
    this.router.navigate(['/events']);
  }

}
