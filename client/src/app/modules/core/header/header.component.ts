import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../login/user.model';
import { AuthService } from './../login/auth.service';
import { UserService } from './../login/user.service';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  activeUser: User;

  constructor(private authService: AuthService,
              private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.userService.getActiveUser().subscribe(
      (user) => this.activeUser = user
    );
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
