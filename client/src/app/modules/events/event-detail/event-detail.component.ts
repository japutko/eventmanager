import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';
import { Event } from './../event.model';

@Component({
  selector: 'app-event-detail',
  styleUrls: ['./event-detail.component.scss'],
  templateUrl: './event-detail.component.html'
})
export class EventDetailComponent implements OnInit {

  event: Event;
  private eventId: number;

  constructor(private eventService: EventService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.eventId = +this.route.snapshot.params['id']; // tslint:disable-line
    this.getEvent();
  }

  duplicate() {
    const navigationExtras = { queryParams: { mode: 'duplicate'}, relativeTo: this.route };
    this.router.navigate(['edit'], navigationExtras);
  }

  delete() {
    this.eventService.deleteEvent(this.event);
    this.router.navigate(['/events']);
  }

  edit() {
    const navigationExtras = { queryParams: { mode: 'edit'}, relativeTo: this.route };
    this.router.navigate(['edit'], navigationExtras);
  }

  goBack() {
    this.router.navigate(['/events']);
  }

  private getEvent(): void {
    this.event = this.eventService.getEventById(this.eventId);
  }

}
