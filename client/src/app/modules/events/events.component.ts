import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../core/login/user.service';
import { User } from './../core/login/user.model';
import { SearchPipe } from './../utils/filters/search/search.pipe';
import { Event } from './event.model';
import { EventService } from './event.service';

@Component({
  selector: 'app-events',
  styleUrls: ['./events.component.scss'],
  templateUrl: './events.component.html'
})
export class EventsComponent implements OnInit {

  shownEvents: Event[] = new Array<Event>();

  private events: Event[];
  private activeUser: User;
  private searchPipe = new SearchPipe();

  // Filters properties
  private timePeriod: number = 0;
  private ownership: string;
  private searchText: string;

  constructor(private route: ActivatedRoute,
              private eventService: EventService,
              private router: Router,
              private userService: UserService) {}

  ngOnInit() {
    this.userService.getActiveUser().subscribe(
      (user) => this.activeUser = user
    );
    this.eventService.getEvents().subscribe(
      (events) => {
        this.events = events;
        this.shownEvents.push(...this.events);
        this.filterEvents();
      }
    );
  }

  createNew(): void {
    const navigationExtras = { queryParams: { mode: 'new' }, relativeTo: this.route };
    this.router.navigate(['new', 'edit'], navigationExtras);
  }

  filterEvents(ownership?: string, searchText?: string, timePeriod?: number): void {
    // Persist values if new comes
    if (ownership) {
      this.ownership = ownership;
    }
    if (searchText) {
      this.searchText = searchText;
    }
    if (timePeriod || timePeriod === 0) {
      this.timePeriod = timePeriod;
    }

    // Prepare array for filtering
    let events: Event[] = new Array<Event>();
    events.push(...this.events);

    // Apply filters one by one
    if (this.ownership === 'my') {
      events = this.showMyEvents(events);
    }
    if (this.ownership === 'others') {
      events = this.showOthersEvents(events);
    }
    if (this.searchText) {
      events = this.searchEvents(events, searchText);
    }
    events = this.filterEventsByTime(events, this.timePeriod);
    this.shownEvents = events;
  }

  searchEvents(events: Event[], expression: string): Event[] {
    return this.searchPipe.transform(events, expression);
  }

  showMyEvents(events: Event[]): Event[] {
    return this.searchPipe.transform(events, this.getActiveUserName(), 'owner');
  }

  showOthersEvents(events: Event[]): Event[] {
    return this.searchPipe.transform(events, this.getActiveUserName(), 'owner', true);
  }

  filterEventsByTime(events: Event[], timePeriod: number): Event[] {
    const today = new Date();
    let condition: (event: Event) => boolean;
    switch (timePeriod) {
      case -1:
        condition = (event: Event) =>
          event.date < today &&
          event.date.getDate() < today.getDate();
        break;
      case 0:
        condition = (event: Event) =>
          (event.date >= today || event.date.getDate() === today.getDate()) &&
          event.date.getFullYear() === today.getFullYear();
        break;
      case 1:
        condition = (event: Event) => event.date.getFullYear() > today.getFullYear();
    }
    return events.filter(condition);
  }

  getActiveUserName(): string {
    return this.activeUser.name;
  }
}
