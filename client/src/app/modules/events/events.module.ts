import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { StepsModule } from 'primeng/steps';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TooltipModule } from 'primeng/tooltip';
import { UtilsModule } from '../utils/utils.module';
import { EventCardComponent } from './event-card/event-card.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventEditComponent } from './event-edit/event-edit.component';
import { EventNavigationComponent } from './event-navigation/event-navigation.component';
import { EventsComponent } from './events.component';

@NgModule({
  declarations: [
    EventsComponent,
    EventCardComponent,
    EventDetailComponent,
    EventEditComponent,
    EventNavigationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // Internal modules
    UtilsModule,
    // PrimeNG modules
    CalendarModule,
    CardModule,
    InputTextModule,
    StepsModule,
    ToggleButtonModule,
    TooltipModule
  ]
})
export class EventsModule { }
