import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Event } from './../event.model';
import { EventService } from './../event.service';

@Component({
  selector: 'app-event-edit',
  styleUrls: ['./event-edit.component.scss'],
  templateUrl: './event-edit.component.html'
})
export class EventEditComponent implements OnInit {

  event: Event;
  minDateValue = new Date();
  editForm: FormGroup;
  formChecked: boolean = true;
  private mode: string;
  private eventId: number;

  constructor(private eventService: EventService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    // tslint:disable
    this.mode = this.route.snapshot.queryParams['mode'];
    this.eventId = +this.route.snapshot.params['id'];
    // tslint:enable
    switch (this.mode) {
      case 'duplicate':
        this.prepareDuplicateEvent();
        break;
      case 'new':
        this.prepareNewEvent();
        break;
      case 'edit':
        this.prepareCurrentEvent();
        break;
      default:
        // Nothing to do
    }
    this.editForm = this.buildForm();
  }

  save(): void {
    if (this.checkForm()) {
      this.populateEventWithFormValues();
      this.eventService.addEvent(this.event);
      this.router.navigate(['/events', this.eventId]);
    }
  }

  discard(): void {
    switch (this.mode) {
      case 'edit':
        this.router.navigate(['..'], { relativeTo: this.route });
        break;
      case 'duplicate':
      case 'new':
        this.router.navigate(['/events']);
    }
  }

  fieldHasError(name: string): boolean {
    return this.editForm.get(name).hasError('required') && this.editForm.get(name).touched;
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      date: new FormControl(this.event.date, [Validators.required]),
      description: new FormControl(this.event.description),
      location: new FormControl(this.event.location, [Validators.required]),
      name: new FormControl(this.event.name, [Validators.required])
    });
  }

  private checkForm(): boolean {
    let ok = true;
    Object.keys(this.editForm.controls).forEach(
      (key) => {
        if (this.editForm.controls[key].invalid) {
          this.editForm.controls[key].markAsTouched();
          ok = false;
        }
      }
    );
    this.formChecked = ok;
    return ok;
  }

  private populateEventWithFormValues(): void {
    this.event.name = this.editForm.get('name').value;
    this.event.date = this.editForm.get('date').value;
    this.event.location = this.editForm.get('location').value;
    this.event.description = this.editForm.get('description').value;
  }

  private prepareCurrentEvent(): void {
    this.event = this.eventService.getEventById(this.eventId);
  }

  private prepareDuplicateEvent(): void {
    this.event = this.eventService.prepareDuplicateOfEventWithId(this.eventId);
  }

  private prepareNewEvent(): void {
    this.event = this.eventService.prepareNewEvent();
    this.eventId = this.event.id;
  }
}
