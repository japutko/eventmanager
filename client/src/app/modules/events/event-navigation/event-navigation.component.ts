import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MenuItem } from 'primeng/api';


@Component({
  selector: 'app-event-navigation',
  styleUrls: ['./event-navigation.component.scss'],
  templateUrl: './event-navigation.component.html'
})
export class EventNavigationComponent implements OnInit {

  // Timeline properties
  items: MenuItem[];
  activeIndex: number = 1;

  // Toggle button properties
  myEvents: boolean = false;
  othersEvents: boolean = false;
  allEvents: boolean = true;
  myEventsLabel = 'My';
  othersEventsLabel = 'Others\'';
  allEventsLabel = 'All';

  @Output() searchExpressionChange: EventEmitter<string> = new EventEmitter();
  @Output() timePeriodChange: EventEmitter<number> = new EventEmitter();
  @Output() ownershipChange: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.createItems();
  }

  timePeriodChanged(): void {
    this.timePeriodChange.emit(this.activeIndex - 1);
  }

  toggleButtons(event: any): void {
    switch (event.originalEvent.target.innerText) {
      case this.allEventsLabel:
        this.myEvents = false;
        this.othersEvents = false;
        this.ownershipChange.emit('all');
        break;
      case this.myEventsLabel:
        this.allEvents = false;
        this.othersEvents = false;
        this.ownershipChange.emit('my');
        break;
      case this.othersEventsLabel:
        this.allEvents = false;
        this.myEvents = false;
        this.ownershipChange.emit('others');
    }
  }

  changeSearchExpression(event: any): void {
    this.searchExpressionChange.emit(event.target.value);
  }

  private createItems(): void {
    this.items = [
      {
        command: () => { this.activeIndex = 0; },
        label: 'Past'
      },
      {
        command: () => { this.activeIndex = 1; },
        label: 'Present'
      },
      {
        command: () => { this.activeIndex = 2; },
        label: 'Future'
      }
    ];
  }

}
