export interface Event {
    date: Date;
    description: string;
    id: number;
    location: string;
    name: string;
    owner: string;
}
