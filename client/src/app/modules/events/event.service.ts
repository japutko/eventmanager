import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import mocks from '../../../mockdata/events.json';
import { User } from '../core/login/user.model.js';
import { UserService } from './../core/login/user.service';
import { Event } from './event.model';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private events: Event[] = [];
  private eventsSubject = new BehaviorSubject<Event[]>(this.events);
  private activeUser: User;

  constructor(private userService: UserService) {
    this.loadEvents();
    this.userService.getActiveUser().subscribe(
      (user) => this.activeUser = user
    );
  }

  getEvents(): Observable<Event[]> {
    return this.eventsSubject.asObservable();
  }

  deleteEvent(event: Event): void {
    const index = this.events.indexOf(event);
    if (index > -1) {
      this.events.splice(index, 1);
    }
    this.eventsSubject.next(this.events);
  }

  addEvent(event: Event): void {
    this.events.push(event);
    this.eventsSubject.next(this.events);
  }

  getEventById(id: number): Event {
    return this.events.find((e) => e.id === id);
  }

  prepareDuplicateOfEventWithId(id: number): Event {
    const original = this.getEventById(id);
    const template = this.prepareNewEvent();
    const now = new Date();
    template.date = original.date < now ? now : original.date;
    template.description = original.description;
    template.location = original.location;
    template.name = original.name;
    return template;
  }

  prepareNewEvent(): Event {
    return {
      date: new Date(),
      description: '',
      id: this.findFreeId(),
      location: '',
      name: '',
      owner: this.activeUser.name
    };
  }

  private findFreeId(): number {
    this.events.sort((a, b) => a.id - b.id);
    let id = 0;
    for (let i = 0; i < this.events.length; i++) {
      if (id === this.events[i].id) {
        id++;
      } else if (id < this.events[i].id) {
        return id;
      }
    }
    return ++id;
  }

  private loadEvents(): void {
    const events = mocks;
    events.forEach(
      (e: any) => {
        const date = e.date.split('.');
        e.date = new Date(date[2], date[1] - 1, date[0]);
      }
    );
    this.events = events;
    this.eventsSubject.next(events);
  }
}
