import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';
import { Event } from './../event.model';

@Component({
  selector: 'app-event-card',
  styleUrls: ['./event-card.component.scss'],
  templateUrl: './event-card.component.html'
})
export class EventCardComponent implements OnInit {

  @Input() event: Event;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private eventService: EventService) { }

  ngOnInit() {
  }

  detail(): void {
    this.router.navigate([this.event.id], { relativeTo: this.route });
  }

  edit(): void {
    const navigationExtras = { queryParams: { mode: 'duplicate' }, relativeTo: this.route };
    this.router.navigate([this.event.id, 'edit'], navigationExtras);
  }

  delete(): void {
    this.eventService.deleteEvent(this.event);
  }
}
