import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: any[],
            searchText: string,
            propertyName?: string,
            antiSearch?: boolean): any[] {
    if (!items) { return []; }
    if (!searchText) { return items; }

    searchText = searchText.toLowerCase();
    let filterFunction: (item: any) => boolean;

    if (propertyName && propertyName in items[0]) {
      filterFunction =
        (item: any) => {
          if (!antiSearch) {
            return String(item[propertyName]).toLowerCase().includes(searchText);
          } else {
            return !String(item[propertyName]).toLowerCase().includes(searchText);
          }
        };
    } else {
      filterFunction =
        (item: any) => {
          if (!antiSearch) {
            return Object.keys(item).some(
              (k) => String(item[k]).toLowerCase().includes(searchText));
          } else {
            return Object.keys(item).every(
              (k) => !String(item[k]).toLowerCase().includes(searchText));
          }
        };
    }

    return items.filter(filterFunction);
  }

}
