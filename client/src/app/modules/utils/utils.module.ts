import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SearchPipe } from './filters/search/search.pipe';

@NgModule({
  declarations: [
    SearchPipe
  ],
  exports: [
    SearchPipe
  ],
  imports: [
    CommonModule
  ]
})
export class UtilsModule { }
